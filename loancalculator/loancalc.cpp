#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <conio.h>
#include "loanfns.h"

#define ENTERPRINCIPAL 1 
#define ENTERRATE 2
#define ENTERDURATION 3
#define CALCPAYMENT 4
#define DISPLAYTABLE 5
#define QUIT 0


int main(void)
{	
	//initialize variables
	int choice;
	double principal = 0, interestrate = 0, loanpayment = 0, payment = 0;	
	int durationofloaninyrs = 0;
	choice = menu(principal, interestrate, durationofloaninyrs);	
	//get input after choosing menu choice
	while (choice != QUIT)
	{			

		switch (choice)
		{
			case ENTERPRINCIPAL:
				printf("Pleae enter principal. ");
				scanf("%lf", &principal);
				break;
			case ENTERRATE:
				printf("Pleae enter interest rate. ");
				scanf("%lf", &interestrate);
				break;
			case ENTERDURATION:
				printf("Pleae enter duration in years. ");
				scanf("%d", &durationofloaninyrs);
				break;
			case CALCPAYMENT:
				if (principal > 0 && interestrate > 0 && durationofloaninyrs > 0)
				{
					payment = calcloanpayment(principal, interestrate, durationofloaninyrs);
					printf("Your loan payment is %f.\n", payment);
				}
				else
				{
					printf("Please enter all the variables required to calculate your loan.\n");
				}
				break;
			case DISPLAYTABLE:
				if (principal > 0 && interestrate > 0 && durationofloaninyrs > 0)
				{
					displayrepaymenttable(principal, interestrate, durationofloaninyrs);
				}
				else
				{
					printf("Please enter all the variables required to calculate your loan table.\n");
				}
				break;
			case QUIT:
				break;
			default:
				printf("Please enter a valid choice.\n");
				break;
		}
		choice = menu(principal, interestrate, durationofloaninyrs);	
	}			
	return 0;
}

int menu(double principal, double rate, int years)
{
	//display menu choices with current values of input variables
	int choice;
	printf("Big Bank Loan Calculator\n");
	if (principal > 0)
		printf("1. Update principal (currently %.2f)\n", principal);
	else
		printf("1. Update principal (currently not set)\n");
	if (rate > 0)
		printf("2. Update annual interest rate (currently %.2lf%%)\n", rate*100);
	else
		printf("2. Update annual interest rate (currently not set)\n");
	if (years > 0)
		printf("3. Update duration of loan (currently %i)\n", years);
	else
		printf("3. Update duration of loan (currently not set)\n");
	printf("4. Calculate loan payment \n");
	printf("5. Display loan repayment table \n");
	printf("0. quit\n");
	scanf(" %i", &choice);
	return choice;
}

double calcloanpayment(double principal, double rate, int duration)
{
	//calculate loan payment amount
	double payment;
	payment = (principal*rate/12)/(1-pow((1+rate/12),(-duration*12)));
	return payment;
}

void displayrepaymenttable(double principal, double rate, int duration)
{	
	//display formatted loan payment table
	double payment, interestpayment, principalpaid, newbalance = principal, oldbalance = principal, monthlyinterestrate = rate/12;
	int month = 1;
	printf("%-13s%-13s%-13s%-13s%-13s%-13s\n", "Month", "Old Balance", "Payment", "Interest", "Principal", "New Balance");
	while (newbalance > 1)
	{
		payment = calcloanpayment(principal, rate, duration);		
		interestpayment = oldbalance*monthlyinterestrate;
		principalpaid = payment - interestpayment;
		newbalance = oldbalance - principalpaid;		
		printf("%-13i%-13.2lf%-13.2lf%-13.2lf%-13.2lf%-13.2lf\n", month, oldbalance, payment, interestpayment, principalpaid, newbalance);
		month++;
		oldbalance = newbalance;
		//wait for keypress before quitting
		if (month %12 == 1)
		{
			printf("Press any key to continue reading.\n");
			getch();
		}
	}

}

